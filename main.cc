/*
 * Empty C++ Application
 */
#include "xparameters.h"
#include "platform.h"
#include "xil_printf.h"
#include "AxiFifoMM.h"

int main() {
    init_platform();
    xil_printf("Hello World\r\n");


    AxiFifoMM fifo(XPAR_AXI_FIFO_0_DEVICE_ID);
    constexpr unsigned data_words_num = 64;
    std::vector<uint32_t> in_buff;
    int tab[20] = {24, 24, 27, 31, 59, 33, 37, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    for (int i = 0; i < 20; i++){
    	in_buff.push_back(tab[i]);
    }
    xil_printf("punkt 1\r\n");

    fifo.write(in_buff);
    xil_printf("punkt 2\r\n");
    auto out_buff = fifo.read();
    xil_printf("punkt 3\r\n");
    for (unsigned i = 0; i < in_buff.size(); i++) {
        xil_printf("in_buff[%d] = %d\n\r", i, in_buff[i]);
    }
    for (unsigned i = 0; i < out_buff.size(); i++) {
        xil_printf("out_buff[%d] = %d\n\r", i, out_buff[i]);
    }
    cleanup_platform();
    return 0;
}

