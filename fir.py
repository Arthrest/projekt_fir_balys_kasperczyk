from myhdl import Signal, intbv, always, block, delay, instance, StopSimulation, ResetSignal, always_comb, instances
from FIR_python import FIR_Python as myFIR
import axis


@block
def FIR(clock, coef, axis_s_raw, axis_m_filtr):
    """
    """
    # tworzenie tablicy 0 o rozmiarze równym ilości współczynników filtru
    tab = [Signal(intbv()[16:].signed()) for _ in range(len(coef))]
    # convert coef from list to tuple
    coef = tuple(coef)
    bit_shift = 15
    signalMax = 2 ** 15
    signalMin = -1 * signalMax
    result = [Signal(intbv(x)[32:].signed()) for x in range(len(coef))]
    result_final = Signal(intbv(0, min=-1 * 2 ** 31, max=2 ** 31))
    coef_sig = [Signal(intbv(x)[32:].signed()) for x in coef]
    for i in range(len(coef)):
        coef_sig[i].driven = True

    result_sum = Signal(intbv(0, min=-1 * 2 ** 31, max=2 ** 31))

    # wykonaj przy każdym narastającym zboczu zegara
    @always(clock.posedge)
    def filtrate():
        axis_s_raw.tready.next = axis_m_filtr.tready
        axis_m_filtr.tlast.next = axis_s_raw.tlast
        axis_m_filtr.tvalid.next = axis_s_raw.tvalid
        # Note this adds an extra delay! (Group delay N/2+1)
        # pętla po wszystkich współczynnikach filtru wynika ze schematu filtru fir, gdzie wszystkie współczynniki się sumują (poprzesuwane w czasie)
        for ii in range(len(coef)):
            if ii == 0:
                tab[ii].next = axis_s_raw.tdata[16:]
            else:
                tab[ii].next = tab[ii - 1]
            result[ii].next = tab[ii] * coef_sig[ii]
        result_final.next = result_sum

        axis_m_filtr.tdata.next = (result_final >> bit_shift)

    @always_comb
    def accumulator():
        result_sum.next = result[0] + result[1]

    return filtrate, accumulator


@block
def TEST():
    HALF_PERIOD = delay(10)

    reset = ResetSignal(0, active=0, async=False)
    clock = Signal(bool(0))
    axis_raw = axis.Axis(32)
    axis_sum = axis.Axis(32)

    # length of filter (number of coefficients) - liczba współczynników filtru
    N = 2
    # cutoff frequency of filter
    cut_off = 0.5

    # word size - ilo bitowy będzie sygnał
    signalMax = 2 ** 15
    signalMin = -1 * signalMax

    # Create filter
    our_fir = myFIR(N, cut_off)
    # our_fir.plot_filter()

    # initial value of signals input and output (both 0)
    print(our_fir.coefficients)
    rounded_coef = list(map(int, our_fir.coefficients * signalMax))
    print(rounded_coef)
    fir_filter = FIR(clock, rounded_coef, axis_raw, axis_sum)

    @always(HALF_PERIOD)
    def clockGen():
        clock.next = not clock

    @instance
    def stimulus_write():
        yield clock.negedge

        tab = [24, 27, 31, 59, 33, 37, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        print("sigin       sigout     ")
        w = 0
        while (w < len(tab)):
            axis_raw.tvalid.next = 1
            axis_raw.tdata.next = tab[w]

            if w == len(tab) - 1:
                axis_raw.tlast.next = 1
            else:
                axis_raw.tlast.next = 0
            if axis_raw.tready == 1:
                w += 1
            yield clock.negedge
        axis_raw.tvalid.next = 0

    @instance
    def stim_rd():
        yield clock.negedge
        while True:
            axis_sum.tready.next = 1
            if axis_sum.tvalid == 1:
                print("  %d         %d" % (axis_raw.tdata, axis_sum.tdata))
                if axis_sum.tlast == 1:
                    break
            yield clock.negedge
        yield delay(200)
        raise StopSimulation()

    fir_filter.convert(hdl="VHDL", initial_values= True)

    return instances()


tb = TEST()
tb.config_sim(trace=True)
tb.run_sim()
