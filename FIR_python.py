from scipy import signal
import numpy as np
# import matplotlib.pyplot as plt


class FIR_Python:
    """
        class includes filters as:
        b -> coefficients
        h -> impulse_response
        w -> frequency domain
    """

    def __init__(self, order, cut_off):
        self.coefficients = signal.firwin(order, cut_off)
        self.frequency_domain, self.impulse_response = signal.freqz(self.coefficients)

    # def plot_filter(self):
    #     self.frequency_domain = self.frequency_domain / np.pi
    #
    #     plt.plot(self.frequency_domain, 20 * np.log10(np.abs(self.impulse_response)))
    #     plt.grid()
    #     plt.show()

